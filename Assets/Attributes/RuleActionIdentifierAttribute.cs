﻿using UnityEngine;

public class RuleActionIdentifierAttribute : PropertyAttribute
{
    public readonly string Label;
    public RuleActionIdentifierAttribute(string label = null)
    {
        this.Label = label;
    }
}