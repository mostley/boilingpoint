﻿using UnityEngine;

public class RuleEventIdentifierAttribute : PropertyAttribute
{
    public readonly string Label;
    public RuleEventIdentifierAttribute(string label = null)
    {
        this.Label = label;
    }
}