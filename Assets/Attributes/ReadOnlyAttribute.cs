﻿using UnityEngine;

public class ReadOnlyAttribute : PropertyAttribute
{
    public readonly string Label;
    public ReadOnlyAttribute(string label=null)
    {
        this.Label = label;
    }
}
