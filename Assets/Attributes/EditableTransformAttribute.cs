﻿using UnityEngine;

public class EditableTransformAttribute : PropertyAttribute
{
    public readonly string Label;
    public EditableTransformAttribute(string label = null)
    {
        this.Label = label;
    }
}
