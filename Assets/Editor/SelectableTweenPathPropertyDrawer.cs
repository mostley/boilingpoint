﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using UnityEditor;
using UnityEngine;

[CustomPropertyDrawer(typeof(SelectableTweenPathAttribute))]
public class SelectableTweenPathPropertyDrawer : PropertyDrawer
{
    const int ElemHeight = 22;
    const int RefreshWidth = 55;

    private SelectableTweenPathAttribute SelectableTweenPathAttribute { get { return ((SelectableTweenPathAttribute)attribute); } }

    private static List<string> s_PathsList = new List<string>();
    private static bool s_Initialized = false;

    private static void Initialize(SerializedProperty property)
    {
        s_PathsList = new List<string>();

        var parent = GetParent(property);
        var fieldInfo = parent.GetType().GetField("Target");
        var target = (Transform)fieldInfo.GetValue(parent);
        if (target != null)
        {
            var paths = target.GetComponents<iTweenPath>();

            foreach (var path in paths)
            {
                s_PathsList.Add(path.pathName);
            }
        }

        s_Initialized = true;
    }

    public override float GetPropertyHeight(SerializedProperty prop, GUIContent label)
    {
        return ElemHeight;
    }

    public override void OnGUI (Rect position, SerializedProperty property, GUIContent label)
    {
        var buttonPos = position;
        buttonPos.x = position.width - RefreshWidth;
        buttonPos.width = RefreshWidth;
        if (GUI.Button(buttonPos, "Refresh"))
        {
            s_Initialized = false;
        }

        if (!s_Initialized)
        {
            Initialize(property);
        }

        if (s_PathsList.Count > 0)
        {
            var popupPosition = position;
            popupPosition.width = position.width - RefreshWidth;
            DrawPopup(popupPosition, property, label);
        }
        else
        {
            EditorGUI.HelpBox(EditorGUI.IndentedRect(position), "No Paths defined", MessageType.Error);
        }
    }

    private static void DrawPopup(Rect position, SerializedProperty property, GUIContent label)
    {
        var indexOfSelectedValue = s_PathsList.IndexOf(property.stringValue);
        if (indexOfSelectedValue == -1)
        {
            indexOfSelectedValue = 0;
        }
        
        EditorGUI.BeginChangeCheck();
        position.height = ElemHeight;
        indexOfSelectedValue = EditorGUI.Popup(position, label, indexOfSelectedValue,
                                               s_PathsList.Select(e => new GUIContent(e)).ToArray());
        string value = null;
        if (indexOfSelectedValue != -1)
        {
            value = s_PathsList[indexOfSelectedValue];
        }
        if (EditorGUI.EndChangeCheck())
        {
            property.stringValue = value;
        }

    }

    public static object GetParent(SerializedProperty prop)
    {
        var path = prop.propertyPath.Replace(".Array.data[", "[");
        object obj = prop.serializedObject.targetObject;
        var elements = path.Split('.');
        foreach (var element in elements.Take(elements.Length - 1))
        {
            if (element.Contains("["))
            {
                var elementName = element.Substring(0, element.IndexOf("[", StringComparison.Ordinal));
                var index = Convert.ToInt32(element.Substring(element.IndexOf("[", StringComparison.Ordinal)).Replace("[", "").Replace("]", ""));
                obj = GetValue(obj, elementName, index);
            }
            else
            {
                obj = GetValue(obj, element);
            }
        }
        return obj;
    }

    public static object GetValue(object source, string name)
    {
        if (source == null)
        {
            return null;
        }
        var type = source.GetType();
        var f = type.GetField(name, BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.Instance);
        if (f == null)
        {
            var p = type.GetProperty(name, BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.Instance | BindingFlags.IgnoreCase);
            if (p == null)
                return null;
            return p.GetValue(source, null);
        }
        return f.GetValue(source);
    }

    public static object GetValue(object source, string name, int index)
    {
        var enumerable = GetValue(source, name) as IEnumerable;
        if (enumerable != null)
        {
            var enm = enumerable.GetEnumerator();
            while (index-- >= 0)
            {
                enm.MoveNext();
            }
            return enm.Current;
        }

        return null;
    }
}
