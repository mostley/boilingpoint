﻿using System;
using System.Collections;
using System.Linq;
using System.Reflection;
using UnityEditor;
using UnityEngine;

[CustomPropertyDrawer(typeof(EditableTransformAttribute))]
public class EditableTransformPropertyDrawer : PropertyDrawer
{
    const int ElemHeight = 22;

    public bool FoldOut = true;

    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
    {
        position.x -= 12;

        EditorGUI.BeginProperty(position, label, property);

        var labelPosition = position;
        labelPosition.width = position.width - position.width/2;
        labelPosition.height = ElemHeight;
        this.FoldOut = EditorGUI.Foldout(labelPosition, this.FoldOut, label);

        if (FoldOut)
        {
            var action = (SetTransformAction)GetParent(property);
            var data = this.GetTransformData(property, action);

            var elemsPosition = position;
            elemsPosition.x = 30;
            elemsPosition.height = ElemHeight;

            elemsPosition.y += ElemHeight;
            data.isLocal = EditorGUI.Toggle(elemsPosition, "Is Local", data.isLocal);

            elemsPosition.y += ElemHeight;
            data.position = EditorGUI.Vector3Field(elemsPosition, "Position", data.position);

            elemsPosition.y += ElemHeight * 2;
            data.rotation = EditorGUI.Vector3Field(elemsPosition, "Rotation", data.rotation);

            elemsPosition.y += ElemHeight * 2;
            data.scale = EditorGUI.Vector3Field(elemsPosition, "Scale", data.scale);

            elemsPosition.y += ElemHeight * 2;
            data.showGizmo = EditorGUI.Toggle(elemsPosition, "Show Gizmo", data.showGizmo);

            var copyFromTargetPosition = position;
            copyFromTargetPosition.x = position.width - position.width / 4;
            copyFromTargetPosition.width = position.width / 4;
            copyFromTargetPosition.height = ElemHeight;
            if (GUI.Button(copyFromTargetPosition, "Copy from Target"))
            {
                data.position = action.Target.position;
                data.rotation = action.Target.rotation.eulerAngles;
                data.scale = action.Target.lossyScale;
            }

            var setToTargetPosition = position;
            setToTargetPosition.x = position.width - position.width / 2;
            setToTargetPosition.width = position.width / 4;
            setToTargetPosition.height = ElemHeight;
            if (GUI.Button(setToTargetPosition, "Set to Target"))
            {
                action.Target.position = data.position;
                action.Target.rotation = Quaternion.Euler(data.rotation);
                action.Target.localScale = data.scale;
            }
        }
    }

    private TransformData GetTransformData(SerializedProperty property, object parent)
    {
        var fieldInfo = parent.GetType().GetField(property.name);
        return (TransformData)fieldInfo.GetValue(parent);
    }

    public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
    {
        return this.FoldOut ? ElemHeight * 9 : ElemHeight;
    }

    public object GetParent(SerializedProperty prop)
    {
        var path = prop.propertyPath.Replace(".Array.data[", "[");
        object obj = prop.serializedObject.targetObject;
        var elements = path.Split('.');
        foreach (var element in elements.Take(elements.Length - 1))
        {
            if (element.Contains("["))
            {
                var elementName = element.Substring(0, element.IndexOf("[", StringComparison.Ordinal));
                var index = Convert.ToInt32(element.Substring(element.IndexOf("[", StringComparison.Ordinal)).Replace("[", "").Replace("]", ""));
                obj = GetValue(obj, elementName, index);
            }
            else
            {
                obj = GetValue(obj, element);
            }
        }
        return obj;
    }

    public object GetValue(object source, string name)
    {
        if (source == null)
        {
            return null;
        }
        var type = source.GetType();
        var f = type.GetField(name, BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.Instance);
        if (f == null)
        {
            var p = type.GetProperty(name, BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.Instance | BindingFlags.IgnoreCase);
            if (p == null)
                return null;
            return p.GetValue(source, null);
        }
        return f.GetValue(source);
    }

    public object GetValue(object source, string name, int index)
    {
        var enumerable = GetValue(source, name) as IEnumerable;
        if (enumerable != null)
        {
            var enm = enumerable.GetEnumerator();
            while (index-- >= 0)
            {
                enm.MoveNext();
            }
            return enm.Current;
        }

        return null;
    }
}
