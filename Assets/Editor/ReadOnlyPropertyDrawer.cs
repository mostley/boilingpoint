﻿using UnityEditor;
using UnityEngine;
using System.Collections;

[CustomPropertyDrawer(typeof(ReadOnlyAttribute))]
public class ReadOnlyPropertyDrawer : PropertyDrawer
{
    private GUIStyle textStyle = new GUIStyle();

    private ReadOnlyAttribute ReadOnlyAttribute { get { return ((ReadOnlyAttribute)attribute); } }

    public ReadOnlyPropertyDrawer()
    {
        textStyle.normal.textColor = Color.gray;
    }

    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
    {
        string propLabel = string.IsNullOrEmpty(ReadOnlyAttribute.Label) ? label.text : ReadOnlyAttribute.Label;
        EditorGUI.LabelField(position, propLabel, property.stringValue, textStyle);
    }
}
