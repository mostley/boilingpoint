﻿using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEngine;

[CustomPropertyDrawer(typeof(RuleActionIdentifierAttribute))]
public class RuleActionIdentifierListPropertyDrawer : PropertyDrawer
{
    const int ElemHeight = 22;
    const int ActionElemHeight = 22;

    private RuleActionIdentifierAttribute RuleActionIdentifierAttribute { get { return ((RuleActionIdentifierAttribute)attribute); } }

    private static bool s_RulesManagerMissing = false;
    private static bool s_Initialized = false;

    private static List<Action> s_ActionsList = new List<Action>();
    private readonly List<bool> arrayFoldout = new List<bool>();

    private static void Initialize()
    {
        s_ActionsList = new List<Action>();

        var rulesManager = GetRulesManager();
        s_RulesManagerMissing = rulesManager == null;

        if (rulesManager != null)
        {
            if (rulesManager.EventsContainer != null)
            {
                var actionComponents = rulesManager.ActionsContainer.GetComponents<Action>();
                foreach (var e in actionComponents)
                {
                    s_ActionsList.Add(e);
                }
            }
            else
            {
                Debug.LogError("ActionsContainer is not defined");
            }
        }
        else
        {
            Debug.LogError("RulesManager is not defined");
        }

        s_Initialized = true;
    }

    private static RulesManager GetRulesManager()
    {
        var managers = GameObject.FindGameObjectWithTag("Managers");
        RulesManager result = null;
        if (managers != null)
        {
            result = managers.GetComponent<RulesManager>();
        }

        return result;
    }

    public override void OnGUI (Rect position, SerializedProperty property, GUIContent label)
    {
        label = string.IsNullOrEmpty(RuleActionIdentifierAttribute.Label) ? label : new GUIContent(RuleActionIdentifierAttribute.Label);

        this.EnsureFoldoutLength(property);

        DrawRefreshButton(position);

        if (!s_Initialized)
        {
            Initialize();
        }

        if (!s_RulesManagerMissing && s_ActionsList.Count > 0)
        {
            var labelPosition = position;
            labelPosition.height = ElemHeight;
            labelPosition.width = position.width / 2;
            EditorGUI.LabelField(labelPosition, label);
            DrawAddAction(position, property);

            var actionPosition = EditorGUI.IndentedRect(position);
            var lastElemHeight = ElemHeight;
            for (int i = 0; i < property.arraySize; i++)
            {
                actionPosition.y += lastElemHeight;
                actionPosition.height = lastElemHeight = arrayFoldout[i] ? ActionElemHeight : ElemHeight;
                var arrayPoperty = property.GetArrayElementAtIndex(i);
                DrawActionElem(actionPosition, property, arrayPoperty, i);
            }
        }
        else
        {
            EditorGUI.HelpBox(EditorGUI.IndentedRect(position), "RulesManager is missing or no Actions defined", MessageType.Error);
        }
    }

    private void DrawActionElem(Rect position, SerializedProperty parentProperty, SerializedProperty property, int index)
    {
        var subPropertyPosition = position;
        subPropertyPosition.width -= 44;
        subPropertyPosition.height = ElemHeight;
        DrawPopup(subPropertyPosition, property, new GUIContent("Action " + index));

        DrawRemoveAction(position, parentProperty, index);
    }

    private static void DrawPopup(Rect position, SerializedProperty property, GUIContent label)
    {
        var listOfIdentifiers = s_ActionsList.Select(r => r.Name).ToList();
        var indexOfSelectedValue = listOfIdentifiers.IndexOf(property.stringValue);
        if (indexOfSelectedValue == -1)
        {
            indexOfSelectedValue = 0;
        }

        EditorGUI.BeginChangeCheck();
        position.height = ElemHeight;
        indexOfSelectedValue = EditorGUI.Popup(position, label, indexOfSelectedValue,
                                               s_ActionsList.Select(e => new GUIContent(e.Name)).ToArray());
        string value = null;
        if (indexOfSelectedValue != -1)
        {
            value = s_ActionsList[indexOfSelectedValue].Name;
        }
        if (EditorGUI.EndChangeCheck())
        {
            property.stringValue = value;
        }

    }

    public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
    {
        if (!s_RulesManagerMissing && s_ActionsList.Count > 0)
        {
            this.EnsureFoldoutLength(property);

            var result = ElemHeight;

            for (int i = 0; i < property.arraySize; i++)
            {
                result += this.arrayFoldout[i] ? ActionElemHeight : ElemHeight;
            }
            return result;
        }
        
        return ElemHeight * 2;
    }

    private void EnsureFoldoutLength(SerializedProperty property)
    {
        while (this.arrayFoldout.Count < property.arraySize)
        {
            this.arrayFoldout.Add(true);
        }
    }

    private static void DrawRefreshButton(Rect position)
    {
        var buttonPos = position;
        buttonPos.x = buttonPos.width/2 + buttonPos.width/4;
        buttonPos.width = buttonPos.width/4;
        buttonPos.height = ElemHeight;
        if (GUI.Button(buttonPos, "Refresh"))
        {
            s_Initialized = false;
        }
    }

    private void DrawAddAction(Rect position, SerializedProperty property)
    {
        var buttonPos = position;
        buttonPos.x = buttonPos.width/2;
        buttonPos.width = buttonPos.width/4;
        buttonPos.height = ElemHeight;
        if (GUI.Button(buttonPos, "Add Action"))
        {
            property.InsertArrayElementAtIndex(property.arraySize);
            arrayFoldout.Add(true);
        }
    }

    private void DrawRemoveAction(Rect position, SerializedProperty property, int index)
    {
        var buttonPos = position;
        buttonPos.x = buttonPos.width - 22;
        buttonPos.y += 4;
        buttonPos.width = 22;
        buttonPos.height = 16;
        if (GUI.Button(buttonPos, "x"))
        {
            property.DeleteArrayElementAtIndex(index);
            arrayFoldout.RemoveAt(index);
        }
    }
}
