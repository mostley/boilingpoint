﻿using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEngine;

[CustomPropertyDrawer(typeof(RuleEventIdentifierAttribute))]
public class RuleEventIdentifierPropertyDrawer : PropertyDrawer
{
    const int ElemHeight = 22;
    const int RefreshWidth = 55;

    private RuleEventIdentifierAttribute RuleEventIdentifierAttribute { get { return ((RuleEventIdentifierAttribute)attribute); } }

    private static List<RuleEvent> s_EventsList = new List<RuleEvent>();
    private static bool s_RulesManagerMissing = false;
    private static bool s_Initialized = false;

    private static void Initialize()
    {
        s_EventsList = new List<RuleEvent>();

        var rulesManager = GetRulesManager();
        s_RulesManagerMissing = rulesManager == null;

        if (rulesManager != null)
        {
            if (rulesManager.EventsContainer != null)
            {
                var eventComponents = rulesManager.EventsContainer.GetComponents<RuleEvent>();
                foreach (var e in eventComponents)
                {
                    s_EventsList.Add(e);
                }
            }
            else
            {
                Debug.LogError("EventsContainer is not defined");
            }
        }
        else
        {
            Debug.LogError("RulesManager is not defined");
        }

        s_Initialized = true;
    }

    private static RulesManager GetRulesManager()
    {
        var managers = GameObject.FindGameObjectWithTag("Managers");
        RulesManager result = null;
        if (managers != null)
        {
            result = managers.GetComponent<RulesManager>();
        }

        return result;
    }

    public override float GetPropertyHeight(SerializedProperty prop, GUIContent label)
    {
        return ElemHeight;
    }

    public override void OnGUI (Rect position, SerializedProperty property, GUIContent label)
    {
        label = string.IsNullOrEmpty(RuleEventIdentifierAttribute.Label) ? label : new GUIContent(RuleEventIdentifierAttribute.Label);

        var buttonPos = position;
        buttonPos.x = buttonPos.width - RefreshWidth;
        buttonPos.width = RefreshWidth;
        buttonPos.height = ElemHeight;
        if (GUI.Button(buttonPos, "Refresh"))
        {
            s_Initialized = false;
        }

        if (!s_Initialized)
        {
            Initialize();
        }

        if (!s_RulesManagerMissing && s_EventsList.Count > 0)
        {
            var popupPosition = position;
            popupPosition.width = position.width - RefreshWidth;
            DrawPopup(popupPosition, property, label);
        }
        else
        {
            EditorGUI.HelpBox(EditorGUI.IndentedRect(position), "RulesManager is missing or no Events defined", MessageType.Error);
        }
    }

    private static void DrawPopup(Rect position, SerializedProperty property, GUIContent label)
    {
        var listOfIdentifiers = s_EventsList.Select(r => r.Name).ToList();
        var indexOfSelectedValue = listOfIdentifiers.IndexOf(property.stringValue);
        if (indexOfSelectedValue == -1)
        {
            indexOfSelectedValue = 0;
        }
        
        EditorGUI.BeginChangeCheck();
        position.height = ElemHeight;
        indexOfSelectedValue = EditorGUI.Popup(position, label, indexOfSelectedValue,
                                               s_EventsList.Select(e => new GUIContent(e.Name)).ToArray());
        string value = null;
        if (indexOfSelectedValue != -1)
        {
            value = s_EventsList[indexOfSelectedValue].Name;
        }
        if (EditorGUI.EndChangeCheck())
        {
            property.stringValue = value;
        }

    }
}
