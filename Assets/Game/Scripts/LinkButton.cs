using UnityEngine;
using System.Collections;

public class LinkButton : Button
{
    public string Url = "http://shdev.de/";

    protected override void OnClick()
    {
        Application.OpenURL(Url);
    }
}
