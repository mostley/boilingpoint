using UnityEngine;

public class SituationalStatistics : MonoBehaviour
{
    public float Stress { get; set; }
    public float PerceivedDangerOfCharacter { get; set; }
    public float PerceivedCoolnesOfCharacter { get; set; } //Popularity
    public float Famousness { get; set; }
}
