﻿using System.Collections;
using UnityEngine;

public class AudioManager : MonoBehaviour
{
    private GameObject backgroundMusicGameObject;

    public void FadeBackgroundMusic(AudioClip clip, float time)
    {
        if (backgroundMusicGameObject == null)
        {
            backgroundMusicGameObject = new GameObject("Background Music");
            backgroundMusicGameObject.transform.position = transform.position;
            backgroundMusicGameObject.transform.parent = transform;
        }

        var currentTrack = backgroundMusicGameObject.GetComponent<AudioSource>();

        //Create the source
        var source = backgroundMusicGameObject.AddComponent<AudioSource>();
        source.clip = clip;
        source.volume = 0f;
        source.pitch = 1f;
        source.loop = true;
        source.rolloffMode = AudioRolloffMode.Linear;
        source.minDistance = 999;
        source.spread = 360;
        source.panLevel = 0;
        source.dopplerLevel = 0;
        source.Play();

        if (currentTrack != null)
        {
            //fade out
            iTween.AudioTo(backgroundMusicGameObject, new Hashtable
                {
                    {iT.AudioTo.audiosource, currentTrack},
                    {iT.AudioTo.time, time},
                    {iT.AudioTo.volume, 0f},
                    {iT.AudioTo.oncomplete, "OnBackgroundMusicFadeOut"},
                    {iT.AudioTo.oncompleteparams, currentTrack},
                });
        }

        //fade in
        iTween.AudioTo(backgroundMusicGameObject, new Hashtable
            {
                {iT.AudioTo.audiosource, source},
                {iT.AudioTo.time, time},
                {iT.AudioTo.volume, 1f},
            });
    }

    protected void OnBackgroundMusicFadeOut(AudioSource currentTrack)
    {
        Destroy(currentTrack);
    }

    public AudioSource Play(AudioClip clip, Transform emitter, bool loop)
    {
        return Play(clip, emitter, 1f, 1f, loop);
    }

    public AudioSource Play(AudioClip clip, Transform emitter, float volume, bool loop)
    {
        return Play(clip, emitter, volume, 1f, loop);
    }

    public AudioSource Play(AudioClip clip, Transform emitter, float volume, float pitch, bool loop)
    {
        //Create an empty game object
        var go = new GameObject("Audio: " + clip.name);
        go.transform.position = emitter.position;
        go.transform.parent = emitter;

        //Create the source
        var source = go.AddComponent<AudioSource>();
        source.clip = clip;
        source.volume = volume;
        source.pitch = pitch;
        source.loop = loop;
        source.Play();
        if (!loop)
        {
            Destroy(go, clip.length);
        }
        return source;
    }

    public AudioSource Play(AudioClip clip, Vector3 point, bool loop)
    {
        return Play(clip, point, 1f, 1f, loop);
    }

    public AudioSource Play(AudioClip clip, Vector3 point, float volume, bool loop)
    {
        return Play(clip, point, volume, 1f, loop);
    }

    public AudioSource Play(AudioClip clip, Vector3 point, float volume, float pitch, bool loop)
    {
        //Create an empty game object
        var go = new GameObject("Audio: " + clip.name);
        go.transform.position = point;

        //Create the source
        var source = go.AddComponent<AudioSource>();
        source.clip = clip;
        source.volume = volume;
        source.pitch = pitch;
        source.loop = loop;
        source.Play();
        if (!loop)
        {
            Destroy(go, clip.length);
        }
        return source;
    }
}