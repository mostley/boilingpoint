using UnityEngine;

public class CharacterStatistics : MonoBehaviour
{
    public float Fear { get; set; }
    public float Insanity { get; set; }
    public float Health { get; set; }
    public float PowerTraining { get; set; }
    public float PhysicalTraining { get; set; }
    public float FightingSkills { get; set; }
    public float Intelligence { get; set; }
    public float Confidence { get; set; }
}
