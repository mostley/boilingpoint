using UnityEngine;

public class Managers : MonoBehaviour
{
    private static int instanceCounter = 0;

    private GameManager game;
    private StatisticsManager statistics;
    private AudioManager audioManager;
    private RulesManager rulesManager;

    public void Awake ()
	{
	    instanceCounter++;
        if (instanceCounter > 1)
        {
            instanceCounter--;
            Destroy(this);
        }

        DontDestroyOnLoad(gameObject);
        Instance = this;
	}

    public static Managers Instance { get; set; }

    public GameManager Game
    {
        get
        {
            if (!this.game)
            {
                this.game = this.GetComponent<GameManager>();
            }
            return this.game;
        }
    }

    public StatisticsManager Statistics
    {
        get
        {
            if (!this.statistics)
            {
                this.statistics = this.GetComponent<StatisticsManager>();
            }
            return this.statistics;
        }
    }

    public AudioManager Audio
    {
        get
        {
            if (!this.audioManager)
            {
                this.audioManager = this.GetComponent<AudioManager>();
            }
            return this.audioManager;
        }
    }

    public RulesManager Rules
    {
        get
        {
            if (!this.rulesManager)
            {
                this.rulesManager = this.GetComponent<RulesManager>();
            }
            return this.rulesManager;
        }
    }
}
