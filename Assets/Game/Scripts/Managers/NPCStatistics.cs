using UnityEngine;

public class NPCStatistics : MonoBehaviour
{
    public float Fear { get; set; }
    public float Anger { get; set; }
    public float Confusion { get; set; }
    public float Shock { get; set; }
    public float Health { get; set; }
    public float Fitness { get; set; }
    public NPCTypes Type { get; set; }
}
