using UnityEngine;
using System.Collections;

public class StatisticsManager : MonoBehaviour
{
    public CharacterStatistics Character { get; set; }
    public SituationalStatistics Situation { get; set; }
}
