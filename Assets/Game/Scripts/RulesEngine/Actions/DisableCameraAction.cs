﻿using UnityEngine;

public class DisableCameraAction : Action
{
    public Camera Target;

    public override void Execute(EventResult result)
    {
        this.Target.enabled = false;
    }
}