﻿using UnityEngine;

public class FadeToMusicAction : Action
{
    public AudioClip Clip;
    public float Time = 1f;

    public override void Execute(EventResult result)
    {
        Managers.Instance.Audio.FadeBackgroundMusic(Clip, Time);
    }
}
