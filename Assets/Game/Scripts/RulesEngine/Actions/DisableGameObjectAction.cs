﻿using UnityEngine;

public class DisableGameObjectAction : Action
{
    public GameObject Target;

    public override void Execute(EventResult result)
    {
        this.Target.SetActive(false);
    }
}