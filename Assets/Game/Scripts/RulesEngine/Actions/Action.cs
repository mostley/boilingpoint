﻿using System.Collections;
using UnityEngine;

public abstract class Action : MonoBehaviour
{
    public string Name;

    public bool Enabled = true;

    public virtual void Awake()
    {
        if (string.IsNullOrEmpty(Name))
        {
            Debug.Log("Rule " + Name + " has no Name (-> destroy).");
            Destroy(gameObject);
            return;
        }

        Managers.Instance.Rules.RegisterAction(this);
    }

    public abstract void Execute(EventResult result);
}
