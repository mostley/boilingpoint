﻿using System.Collections;
using UnityEngine;

public class CinematicBorderSlideOutAction : Action
{
    public GameObject UpperBorder;
    public GameObject LowerBorder;
    public float Time;
    public bool HideAfterwards;

    public override void Execute(EventResult result)
    {
        iTween.MoveBy(UpperBorder, new Hashtable
            {
                {iT.MoveBy.amount, Vector3.up * 0.04f},
                {iT.MoveBy.time, Time},
            });
        iTween.MoveBy(LowerBorder, new Hashtable
            {
                {iT.MoveBy.amount, -Vector3.up * 0.04f},
                {iT.MoveBy.time, Time},
                {iT.MoveBy.oncomplete, "OnSlideoutComplete"},
            });
    }

    public void OnSlideoutComplete()
    {
        if (HideAfterwards)
        {
            UpperBorder.SetActive(false);
            LowerBorder.SetActive(false);
        }
    }
}
