﻿using UnityEngine;

public class StopTweensAction : Action
{
    public Transform Target;

    public override void Execute(EventResult result)
    {
        iTween.Stop(Target.gameObject);
    }
}
