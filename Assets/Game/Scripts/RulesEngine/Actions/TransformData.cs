﻿using System;
using UnityEngine;

public enum IngredientUnit { Spoon, Cup, Bowl, Piece }

[Serializable]
public class TransformData
{
    public Vector3 position;
    public Vector3 rotation;
    public Vector3 scale;
    public bool isLocal;
    public bool showGizmo;
}