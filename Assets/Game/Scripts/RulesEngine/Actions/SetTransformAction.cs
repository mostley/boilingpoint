﻿using System.Collections;
using UnityEngine;

public class SetTransformAction : Action
{
    public Transform Target;

    [EditableTransformAttribute]
    public TransformData Values;

    public override void Execute(EventResult result)
    {
        Target.position = Values.position;
        Target.rotation = Quaternion.Euler(Values.rotation);
        Target.localScale = Values.scale;
    }

    public virtual void OnDrawGizmosSelected()
    {
        if (Values.showGizmo)
        {
            GL.PushMatrix();

            var rotationMatrix = Matrix4x4.TRS(Values.position, Quaternion.Euler(Values.rotation), Vector3.one);

            GL.MultMatrix(rotationMatrix);
            Gizmos.matrix = rotationMatrix;

            Gizmos.DrawWireCube(Vector3.zero, Values.scale * 0.1f);
            Gizmos.DrawRay(Vector3.zero, Vector3.forward * Values.scale.magnitude * 0.1f);

            GL.PopMatrix();
        }
    }
}