﻿using UnityEngine;

public class EnableCameraAction : Action
{
    public Camera Target;

    public override void Execute(EventResult result)
    {
        Target.enabled = true;
    }
}