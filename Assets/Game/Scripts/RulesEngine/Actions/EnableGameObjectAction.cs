﻿using UnityEngine;

public class EnableGameObjectAction : Action
{
    public GameObject Target;

    public override void Execute(EventResult result)
    {
        this.Target.SetActive(true);
    }
}