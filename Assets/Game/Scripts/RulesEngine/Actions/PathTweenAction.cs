﻿using System.Collections;
using UnityEngine;

public class PathTweenAction : Action
{
    public Transform Target;

    [SelectableTweenPathAttribute]
    public string PathName;
    
    public float Time = 1.0f;
    public iTween.LoopType LoopType = iTween.LoopType.none;
    public iTween.EaseType EaseType = iTween.EaseType.linear;

    public override void Execute(EventResult result)
    {
        var path = iTweenPath.GetPath(PathName);
        iTween.MoveTo(Target.gameObject, new Hashtable
            {
                {iT.MoveTo.path, path},
                {iT.MoveTo.time, Time},
                {iT.MoveTo.looptype, LoopType},
                {iT.MoveTo.easetype, EaseType},
            });
    }
}
