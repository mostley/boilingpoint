using System.Collections;
using System.Collections.Generic;

using UnityEngine;

public class RulesManager : MonoBehaviour
{
    private readonly List<Rule> rulesList = new List<Rule>();
    private readonly List<RuleEvent> eventsList = new List<RuleEvent>();
    private readonly List<Action> actionsList = new List<Action>();
    private readonly Dictionary<string, List<Rule>> eventRules = new Dictionary<string, List<Rule>>();
    private readonly Dictionary<string, RuleEvent> events = new Dictionary<string, RuleEvent>();
    private readonly Dictionary<string, Action> actions = new Dictionary<string, Action>();

    public List<RuleEvent> Events { get { return eventsList; } }
    public List<Rule> Rules { get { return rulesList; } }
    public List<Action> Actions { get { return actionsList; } }

    public GameObject RulesContainer;
    public GameObject EventsContainer;
    public GameObject ActionsContainer;

    public void Awake()
    {
    }

    public void Update()
    {
        foreach (var ruleEvent in events.Values)
        {
            //only check if needed
            if (ruleEvent.Enabled && eventRules.ContainsKey(ruleEvent.Name))
            {
                var result = ruleEvent.Check();
                if (result != null)
                {
                    foreach (var rule in eventRules[ruleEvent.Name])
                    {
                        Debug.Log("Rule '" + rule.Name + "' triggered (due to Event '" + ruleEvent.Name + "').");

                        rule.Trigger(this, result);
                    }

                    ruleEvent.Handled();
                }
            }
        }
    }

    public void RegisterRule(Rule rule)
    {
        if (string.IsNullOrEmpty(rule.EventId))
        {
            Debug.LogError("Rule '" + rule.Name + "' does not contain event id.");
        }
        else if (!events.ContainsKey(rule.EventId))
        {
            Debug.LogError("Event '" + rule.EventId + "' not found. Added the GameObject?");
        }
        else
        {
            if (!eventRules.ContainsKey(rule.EventId))
            {
                eventRules.Add(rule.EventId, new List<Rule>());
            }

            eventRules[rule.EventId].Add(rule);
            this.rulesList.Add(rule);
        }
    }

    public void RegisterEvent(RuleEvent ruleEvent)
    {
        if (events.ContainsKey(ruleEvent.Name))
        {
            Debug.LogError("Event '" + ruleEvent.Name + "' already added, forgot to change the id?");
        }
        else
        {
            events.Add(ruleEvent.Name, ruleEvent);
            eventsList.Add(ruleEvent);
        }
    }

    public void RegisterAction(Action action)
    {
        if (actions.ContainsKey(action.Name))
        {
            Debug.LogError("Action '" + action.Name + "' already added, forgot to change the id?");
        }
        else
        {
            actions.Add(action.Name, action);
            actionsList.Add(action);
        }
    }

    public Action GetAction(string actionId)
    {
        Action result = null;

        if (actions.ContainsKey(actionId))
        {
            result = actions[actionId];
        }

        return result;
    }
}