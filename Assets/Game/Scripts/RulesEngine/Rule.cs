using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rule : MonoBehaviour
{
    public string Name;

    public bool Enabled = true;

    [RuleEventIdentifierAttribute]
    public string EventId;

    [RuleActionIdentifierAttribute("Actions")]
    public List<string> ActionIds;

    public virtual void Start()
    {
        if (string.IsNullOrEmpty(EventId))
        {
            Debug.Log("Rule " + EventId + " has no EventID (-> destroy).");
            Destroy(gameObject);
            return;
        }
        if (string.IsNullOrEmpty(Name))
        {
            Debug.Log("Rule " + Name + " has no Name (-> destroy).");
            Destroy(gameObject);
            return;
        }

        Managers.Instance.Rules.RegisterRule(this);
    }

    public virtual void Update()
    {
    }

    public virtual void Trigger(RulesManager ruleManager, EventResult result)
    {
        if (Enabled)
        {
            foreach (var actionId in ActionIds)
            {
                var action = ruleManager.GetAction(actionId);
                if (action != null)
                {
                    Debug.Log("Execute Action '" + action.Name + "'.");
                    action.Execute(result);
                }
                else
                {
                    Debug.LogError("Action '" + actionId + "' is not defined.");
                }
            }
        }
    }
}
