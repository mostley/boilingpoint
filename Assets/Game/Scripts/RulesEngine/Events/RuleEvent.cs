using System;
using UnityEngine;

public class RuleEvent : MonoBehaviour
{
    public string Name;

    public bool Enabled = true;

    protected EventResult eventResult = null;

    public virtual void Awake()
    {
        if (string.IsNullOrEmpty(Name))
        {
            Debug.Log("Rule " + Name + " has no Name (-> destroy).");
            Destroy(gameObject);
            return;
        }

        Managers.Instance.Rules.RegisterEvent(this);
	}
	
	public virtual void Update ()
    {
	
	}

    public EventResult Check()
    {
        return eventResult;
    }

    protected virtual void SetResult(EventResult result)
    {
        this.eventResult = result;
    }

    public virtual void Handled()
    {
        this.Enabled = false;
    }
}
