﻿using UnityEngine;

public class TimedEvent : RuleEvent
{
    private float startTime;
    private float pauseStartTime;
    private bool isPaused;

    public float Duration;

    public override void Awake()
    {
        base.Awake();

        startTime = Time.time;
    }

    public override void Update()
    {
        base.Update();

        if (Enabled)
        {
            if (isPaused)
            {
                isPaused = false;
                startTime += Time.time - pauseStartTime;
            }

            if ((Time.time - startTime) > Duration)
            {
                this.SetResult(new TimedEventResult(Time.time));
            }
        }
        else
        {
            if (!isPaused)
            {
                isPaused = true;
            }
        }
    }
}