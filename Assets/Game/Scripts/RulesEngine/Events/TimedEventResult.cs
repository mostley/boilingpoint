﻿public class TimedEventResult : EventResult
{
    private readonly float time;

    public TimedEventResult(float time)
    {
        this.time = time;
    }

    public float Time
    {
        get { return this.time; }
    }
}