using UnityEngine;

public class Button : MonoBehaviour
{
    public int LevelToLoad = 1;
    public Texture2D CursorTexture;
    public Vector2 CursorHotSpot;

    private bool isOver = false;

    public void Update()
    {
        var ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;
        if (Physics.Raycast(ray, out hit, 999))
        {
            if (hit.collider == collider)
            {
                if (Input.GetMouseButton(0))
                {
                    OnClick();
                }
                else if (!isOver)
                {
                    isOver = true;
                    OnEnter();
                }
            }
        }
        else
        {
            isOver = false;
            OnLeave();
        }
    }

    private void OnEnter()
    {
        Cursor.SetCursor(CursorTexture, CursorHotSpot, CursorMode.Auto);

        if (audio)
        {
            audio.Play();
        }
    }

    private void OnLeave()
    {
        Cursor.SetCursor(null, Vector2.zero, CursorMode.Auto);
    }

    protected virtual void OnClick()
    {
        Cursor.SetCursor(null, Vector2.zero, CursorMode.Auto);
        Application.LoadLevel(LevelToLoad);
    }
}
