public enum NPCTypes
{
    Person = 0,
    Kid = 1,
    Reporter = 2,
    Hero = 3,
    Villain = 4,
    Gangster = 5,
    VIP = 6,
    Police = 7,
}
