using UnityEngine;

public class HeadController : MonoBehaviour
{
    private Vector2 posLeftEyeDefault;
    private Vector2 posLeftEye;
    private Vector2 posRightEyeDefault;
    private Vector2 posRightEye;
    private Vector2 posMouthDefault;
    private Vector2 posMouth;
    private const string TexNameEyeLeft = "_EyeLeftTex";
    private const string TexNameEyeRight = "_EyeRightTex";
    private const string TexNameMouth = "_MouthTex";
    private Renderer headRenderer;

    public Transform LookTarget;
    public GameObject HeadGraphic;
    public float LookHeight = 0.2f;
    public float FOV = 60;

    public void Start()
    {
        headRenderer = HeadGraphic.renderer;
        this.posLeftEyeDefault = posLeftEye = headRenderer.material.GetTextureOffset(TexNameEyeLeft);
        this.posRightEyeDefault = posRightEye = headRenderer.material.GetTextureOffset(TexNameEyeRight);
        this.posMouthDefault = posMouth = headRenderer.material.GetTextureOffset(TexNameMouth);
    }

    public void Update()
    {
        if (Input.GetMouseButtonUp(0))
        {
            var mouseRay = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;
            if (Physics.Raycast(mouseRay, out hit))
            {
                LookTarget.position = hit.point + Vector3.up*LookHeight;
            }
        }

        var headLookDirection = transform.forward;
        var targetVector = LookTarget.position - transform.position;
        var maxLineOfSightAngle = FOV/2f;

        var lineOfSightAngle = Vector3.Angle(headLookDirection, targetVector);
        var percentAngle = lineOfSightAngle/maxLineOfSightAngle;


        if (lineOfSightAngle > maxLineOfSightAngle)
        {
            //can't see the object
        }
        else
        {
            
        }

        var mousePos = new Vector2(Mathf.Clamp(Input.mousePosition.x / Screen.width, 0, 1), Mathf.Clamp(Input.mousePosition.y / Screen.height, 0, 1));

        posLeftEye = posLeftEyeDefault - 0.02f * mousePos;
        posRightEye = posRightEyeDefault - 0.02f * mousePos;

        headRenderer.material.SetTextureOffset(TexNameEyeLeft, posLeftEye);
        headRenderer.material.SetTextureOffset(TexNameEyeRight, posRightEye);
        headRenderer.material.SetTextureOffset(TexNameMouth, posMouth);
    }


    public void OnDrawGizmosSelected()
    {
        Gizmos.DrawLine(transform.position, LookTarget.transform.position);
        Gizmos.color = Color.green;
        Gizmos.DrawWireSphere(LookTarget.transform.position, 0.05f);

        Gizmos.color = Color.blue;
        Gizmos.DrawFrustum(transform.position, FOV, -5, -0.1f, 1);
    }
}
