using UnityEngine;
using System.Collections;

public class Hover : MonoBehaviour
{
	public float RelativeHeight = 1;
	public float HoverIntensity = 1;
	private float absoluteHeight;
	private bool goUp = true;
	
    public void Start()
    {
        iTween.MoveTo(gameObject, new Hashtable
	        {
	            {iT.MoveTo.looptype, iTween.LoopType.none},
	            {iT.MoveTo.time, 2},
	            {iT.MoveTo.easetype, iTween.EaseType.easeInOutBack},
	            {iT.MoveTo.position, transform.position + Vector3.up * RelativeHeight},
				{iT.MoveTo.oncomplete, "OnHeightReached"},
				{iT.MoveTo.oncompletetarget, gameObject},
	        });
		
        iTween.ShakeRotation(gameObject, new Hashtable
	        {
	            {iT.MoveTo.looptype, iTween.LoopType.loop},
	            {iT.ShakeRotation.time, 0.5f},
	            {iT.ShakeRotation.x, 1},
	            {iT.ShakeRotation.y, 1},
	            {iT.ShakeRotation.z, 1},
	        });
    }
	
    public void OnHeightReached()
    {
		float time = 1/HoverIntensity + Random.value;
		
        iTween.MoveTo(gameObject, new Hashtable
	        {
	            {iT.MoveTo.looptype, iTween.LoopType.none},
	            {iT.MoveTo.time, time},
	            {iT.MoveTo.easetype, iTween.EaseType.easeInOutSine},
	            {iT.MoveTo.position, transform.position + Vector3.up * HoverIntensity * (goUp ? -1:1)},
				{iT.MoveTo.oncomplete, "OnHeightReached"},
				{iT.MoveTo.oncompletetarget, gameObject},
	        });
		
		goUp = !goUp;
	}
    
	public void StopHover()
    {
	}
}
