Shader "Custom/Layers" {

Properties
{
	_Color ("Main Color", Color) = (1,1,1)
	_SkinTex ("Skin (RGB)", 2D) = ""
    _EyeLeftTex ("Eye Left (RGBA)", 2D) = "white" {}
    _EyeRightTex ("Eye Right (RGBA)", 2D) = "white" {}
    _MouthTex ("Mouth (RGBA)", 2D) = "white" {}
    _HairTex ("Hair (RGBA)", 2D) = "white" {}
}

SubShader 
{
	Lighting On
	
	Material 
	{
		Ambient [_Color]
		Diffuse [_Color]
	}
	
	Pass
	{		
		SetTexture [_SkinTex]
		{
            combine texture
        }
		
		SetTexture [_EyeLeftTex]
		{
			combine texture lerp (texture) previous
		}
		
		SetTexture [_EyeRightTex]
		{
			combine texture lerp (texture) previous
		}
		
		SetTexture [_MouthTex]
		{
			combine texture lerp (texture) previous
		}
		
		SetTexture [_HairTex]
		{
			combine texture lerp (texture) previous
		}
	}
}

Fallback "Diffuse"

 
}